﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsBtnScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Button btn = gameObject.GetComponent<Button>() as Button;
        btn.onClick.AddListener(SetToggle);
    }

    void SetToggle() {
        SettingsManager manager = GameObject.Find("SettingsManager").GetComponent<SettingsManager>() as SettingsManager;
        manager.SetToggle();
    }
}
