﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSliderScript : MonoBehaviour
{
    void Start()
    {
        Slider slider = gameObject.GetComponent<Slider>() as Slider;
        slider.onValueChanged.AddListener(delegate {SetVolume(slider);});
    }

    void SetVolume(Slider slider) {
        SettingsManager manager = GameObject.Find("SettingsManager").GetComponent<SettingsManager>() as SettingsManager;
        manager.SetVolume(slider.value);
        if(manager.isSoundMute()) {
            manager.Mute();
        }
    }
}
