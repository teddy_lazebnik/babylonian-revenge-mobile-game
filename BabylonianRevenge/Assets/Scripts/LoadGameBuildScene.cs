﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGameBuildScene : MonoBehaviour
{
    public void LoadScene()
    {
        SceneManager.LoadScene(EnumToValue.getValue(SceneNames.GameBuild));
    }
}
