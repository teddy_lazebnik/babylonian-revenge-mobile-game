﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public const float delayCheck = 0.1f;
    public const float collapseFactor = 1.33f;

    public GameObject gameover;
    public GameObject gameWolrd;
    public TextMeshProUGUI score;

    // members //
    public float time;
    public float towerSize;
    public float towerSizeLast;
    public List<GameObject> blocks;
    public List<GameObject> enemys;
    public int enemyKilled;
    public bool isGameBuild;

    public int firstEnemyCreationTime;
    public float EnemyCreationRate;

    public bool createEnemyFlag;
    public float lastEnemyCreatedTime;

    public GameLevel gameLevel;
    public Vector3 centerFloor;
    public bool isGameOver;
    private bool isGameOverMusicOn;
    // end - members //

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        this.time = 0f;
        this.enemyKilled = 0;
        this.towerSize = 0;
        this.towerSizeLast = 0;
        this.isGameOver = false;
        this.blocks = new List<GameObject>();
        this.enemys = new List<GameObject>();

        this.createEnemyFlag = false;
        this.lastEnemyCreatedTime = 0f;
        this.isGameOverMusicOn = false;

        try
        {
            this.gameLevel = GameObject.Find("SettingsManager").GetComponent<SettingsManager>().difficulty;
        }
        catch (Exception error)
        {
            this.gameLevel = GameLevel.Medium;
        }

        switch (this.gameLevel)
        {
            case GameLevel.Easy:
                firstEnemyCreationTime = 30;
                EnemyCreationRate = 5;
                break;
            case GameLevel.Medium:
                firstEnemyCreationTime = 20;
                EnemyCreationRate = 4;
                GameObject.Find("/game/Main Camera/Canvas/pick_panel/pick_btn_4").SetActive(false);
                GameObject.Find("/game/Main Camera/Canvas/pick_panel/pick_btn_5").SetActive(false);
                break;
            case GameLevel.Hard:
                firstEnemyCreationTime = 10;
                EnemyCreationRate = 3;
                GameObject.Find("/game/Main Camera/Canvas/pick_panel/pick_btn_2").SetActive(false);
                GameObject.Find("/game/Main Camera/Canvas/pick_panel/pick_btn_3").SetActive(false);
                GameObject.Find("/game/Main Camera/Canvas/pick_panel/pick_btn_4").SetActive(false);
                GameObject.Find("/game/Main Camera/Canvas/pick_panel/pick_btn_5").SetActive(false);
                break;
        }

        if (this.isGameBuild)
        {
            EnemyCreationRate = 999;
            firstEnemyCreationTime = 999;
            GameObject.Find("/game/Main Camera/Canvas/pick_panel/pick_btn_1").SetActive(true);
            GameObject.Find("/game/Main Camera/Canvas/pick_panel/pick_btn_2").SetActive(true);
            GameObject.Find("/game/Main Camera/Canvas/pick_panel/pick_btn_3").SetActive(true);
            GameObject.Find("/game/Main Camera/Canvas/pick_panel/pick_btn_4").SetActive(true);
            GameObject.Find("/game/Main Camera/Canvas/pick_panel/pick_btn_5").SetActive(true);
        }

        // check if the game ended each 2 seconds - too often is bad
        InvokeRepeating("updateRefferanceSizeToCollapse", this.firstEnemyCreationTime, 2f);
        // update GUI and size fast enoght to make the player informable and not too often to make the process faster
        InvokeRepeating("updateTower", 2f, delayCheck);

        // make gravity stronger to make movement more intresting
        Physics.gravity = new Vector3(0, -30F, 0);

        // TODO: change it to be set when the floor object has been found in the AR
        centerFloor = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (this.time > this.firstEnemyCreationTime)
        {
            this.createEnemyFlag = true;
        }

        if (this.createEnemyFlag && this.time - this.lastEnemyCreatedTime > this.EnemyCreationRate)
        {
            this.lastEnemyCreatedTime = this.time;
            CreateEnemy();
        }
        
        if (this.isGameOver)
        {
                        // show finish menu + stop the clock
            this.score.SetText("Final Score: " + ((float)Math.Round(this.towerSize * 100) / 100).ToString() + "M");
            this.gameover.SetActive(true);
            Time.timeScale = 0f;
            if(!isGameOverMusicOn) {
                this.isGameOverMusicOn = true;
                var audioSource = GameObject.Find("sound_manager").GetComponent<AudioSource>();
                audioSource.clip = Resources.Load<AudioClip>("level_win2-FreeSound");
                audioSource.loop = false;
                audioSource.playOnAwake = false;
                audioSource.Play();
            }
        }
    }

    public void stopClock()
    {
        Time.timeScale = 0f;
    }

    public void resumeClock()
    {
        Time.timeScale = 1f;
    }

    public void updateRefferanceSizeToCollapse()
    {
        this.towerSizeLast = this.towerSize;
    }

    public void AddBlock(GameObject newBlock)
    {
        this.blocks.Add(newBlock);
    }

    public void removeBlock(GameObject block)
    {
        try
        {
            this.blocks.Remove(block);
        }
        catch (Exception error)
        {

        }
    }

    public void removeEnemy(GameObject enemy)
    {
        try
        {
            this.enemys.Remove(enemy);
        }
        catch (Exception error)
        {

        }
    }

    private void updateTower()
    {
        this.towerSize = CalcTowerSize();
        if (CheckCollapse() || (this.blocks.Count == 0 && this.time > this.firstEnemyCreationTime))
        {
            this.isGameOver = true;
        }
    }

    public void countKill(GameObject enemy)
    {
        // remove it and count it
        this.enemys.Remove(enemy);
        this.enemyKilled += 1;
    }

    public float CalcTowerSize()
    {
        float answer = 0f;
        for (int index = 0; index < this.blocks.Count; ++index)
        {
            // check if we can count it or just build on air
            if (!this.blocks[index].GetComponent<BlockColaider>().inPlace)
            {
                continue;
            }
            // check if hieghtest
            float y = this.blocks[index].transform.position.y;
            if (y > answer)
            {
                answer = y;
            }
        }
        return answer;
    }

    public bool CheckCollapse()
    {
        return (this.towerSize * collapseFactor < this.towerSizeLast && this.towerSize != this.towerSizeLast && this.towerSizeLast > 8);
    }

    public void CreateEnemy()
    {
        const int minDistance = 30;
        string prefabName = "";
        float hiehgtByEnemy = 0;
        System.Random random = new System.Random();
        switch (random.Next(27))
        {
            case 0:
            case 19:
            case 23:
                prefabName = "walking_enemy";
                hiehgtByEnemy = 0;
                break;
            case 1:
            case 20:
            case 24:
                prefabName = "walking_human_enemy_red";
                hiehgtByEnemy = 0;
                break;
            case 2:
            case 21:
            case 25:
                prefabName = "walking_human_enemy_black";
                hiehgtByEnemy = 0;
                break;
            case 3:
            case 22:
            case 26:
                prefabName = "walking_human_enemy_blue";
                hiehgtByEnemy = 0;
                break;
            case 4:
                prefabName = "flying_enemy_black";
                hiehgtByEnemy = 0;
                break;
            case 5:
                prefabName = "flying_enemy_green";
                hiehgtByEnemy = 0;
                break;
            case 6:
                prefabName = "flaying_sparrow_enemy_1";
                hiehgtByEnemy = random.Next(2, (int)Math.Floor(this.towerSize));
                break;
            case 7:
                prefabName = "flaying_sparrow_enemy_2";
                hiehgtByEnemy = random.Next(2, (int)Math.Floor(this.towerSize));
                break;
            case 8:
                prefabName = "flaying_sparrow_enemy_3";
                hiehgtByEnemy = random.Next(2, (int)Math.Floor(this.towerSize));
                break;
            case 9:
                prefabName = "flaying_sparrow_enemy_4";
                hiehgtByEnemy = random.Next(2, (int)Math.Floor(this.towerSize));
                break;
            case 10:
                prefabName = "flaying_sparrow_enemy_5";
                hiehgtByEnemy = random.Next(2, (int)Math.Floor(this.towerSize));
                break;
            case 11:
                prefabName = "flaying_sparrow_enemy_6";
                hiehgtByEnemy = random.Next(2, (int)Math.Floor(this.towerSize));
                break;
            case 12:
                prefabName = "flaying_sparrow_enemy_7";
                hiehgtByEnemy = random.Next(2, (int)Math.Floor(this.towerSize));
                break;
            case 13:
                prefabName = "flaying_sparrow_enemy_8";
                hiehgtByEnemy = random.Next(2, (int)Math.Floor(this.towerSize));
                break;
            case 14:
                prefabName = "flaying_sparrow_enemy_9";
                hiehgtByEnemy = random.Next(2, (int)Math.Floor(this.towerSize));
                break;
            case 15:
                prefabName = "flaying_sparrow_enemy_10";
                hiehgtByEnemy = random.Next(2, (int)Math.Floor(this.towerSize));
                break;
            case 16:
                prefabName = "flaying_sparrow_enemy_11";
                hiehgtByEnemy = random.Next(2, (int)Math.Floor(this.towerSize));
                break;
            case 17:
                prefabName = "flaying_sparrow_enemy_12";
                hiehgtByEnemy = random.Next(2, (int)Math.Floor(this.towerSize));
                break;
            case 18:
                prefabName = "flaying_sparrow_enemy_13";
                hiehgtByEnemy = random.Next(2, (int)Math.Floor(this.towerSize));
                break;
            default:
                prefabName = "walking_enemy";
                break;
        }
        // add random noise to location
        Vector3 firstLocation = Vector3.zero;
        int noise = 2;
        do
        {
            noise += 3;
            firstLocation = new Vector3(this.centerFloor.x + random.Next(2 * noise) - noise,
                                    this.centerFloor.y + 0.5f + hiehgtByEnemy,
                                    this.centerFloor.z + random.Next(2 * noise) - noise);
        } while (!farFromAll(firstLocation, this.blocks, minDistance) || !farFromAll(firstLocation, this.enemys, minDistance));
        // load enemy instance
        GameObject instance = Resources.Load("Enemy/" + prefabName, typeof(GameObject)) as GameObject;
        GameObject currentObject = Instantiate(instance, firstLocation, Quaternion.identity);
        currentObject.transform.parent = gameWolrd.transform;
        // add on touch destroy
        currentObject.AddComponent<Lean.Touch.LeanDestroy>();
        // add 
        this.enemys.Add(currentObject);
    }

    public bool farFromAll(Vector3 newObjLocation, List<GameObject> oldObjs, float trashold)
    {
        for (int index = 0; index < oldObjs.Count; index++)
        {
            if (Vector3.Distance(newObjLocation, oldObjs[index].transform.position) < trashold)
            {
                return false;
            }
        }
        return true;
    }
}
