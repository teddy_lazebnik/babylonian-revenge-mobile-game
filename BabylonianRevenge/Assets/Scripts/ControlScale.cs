﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlScale : MonoBehaviour
{
    // members //
    float MIN_SIZE = 0.8f;
    float MAX_SIZE = 1.2f;
    // end - members //

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 scale = gameObject.transform.localScale;
        if (scale.x < MIN_SIZE || scale.y < MIN_SIZE || scale.z < MIN_SIZE)
        {
            gameObject.transform.localScale = new Vector3(MIN_SIZE, MIN_SIZE, MIN_SIZE);
        }
        if (scale.x > MAX_SIZE || scale.y > MAX_SIZE || scale.z > MAX_SIZE)
        {
            gameObject.transform.localScale = new Vector3(MAX_SIZE, MAX_SIZE, MAX_SIZE);
        }
    }
}
