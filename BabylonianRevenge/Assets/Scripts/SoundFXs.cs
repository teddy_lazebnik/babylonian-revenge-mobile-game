﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFXs : MonoBehaviour
{
    public AudioSource audioSource;
    // Start is called before the first frame update
    
    public AudioClip explosionFX;

    public void ExlposionSound() {
        audioSource.PlayOneShot(explosionFX);
    }
}
