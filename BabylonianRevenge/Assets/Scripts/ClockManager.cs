﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClockManager : MonoBehaviour
{
    public const int gameTimeSeconds = 60;

    public GameManager gm;
    public Text time_txt_obj;

    // Start is called before the first frame update
    void Start()
    {
        updateClock();
        InvokeRepeating("updateClock", 1f, 1f);
    }

    public void updateClock()
    {
        int secondsToEnd = gameTimeSeconds - (int)Math.Floor(gm.time);
        if (secondsToEnd < 0)
        {
            gm.isGameOver = true;
            time_txt_obj.text = "0s";
        }
        else
        {
            time_txt_obj.text = secondsToEnd.ToString() + "s";
        }
    }
}
