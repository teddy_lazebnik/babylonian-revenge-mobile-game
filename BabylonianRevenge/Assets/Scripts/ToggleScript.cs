﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Toggle toggle = gameObject.GetComponent<Toggle>() as Toggle;
        toggle.onValueChanged.AddListener(delegate {SetVibrate(toggle);});
    }

    void SetVibrate(Toggle toggle) {
        SettingsManager manager = GameObject.Find("SettingsManager").GetComponent<SettingsManager>() as SettingsManager;
        manager.SetVibration(toggle.isOn);
    }
}
