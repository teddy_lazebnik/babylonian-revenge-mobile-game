using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class Hacks : MonoBehaviour
{
    // members //
    // health keys
    public bool isHpress = false;
    public bool isApress = false;
    public bool isEpress = false;
    public bool isLpress = false;
    public bool isTpress = false;
    // build keys
    public bool isBpress = false;
    public bool isUpress = false;
    public bool isIpress = false;
    public bool isLpress2 = false;
    public bool has_tower = false;
	// tower
	public GameObject secretTower;
    // end - members //
	
	public void Start()
	{
	
	}
	
	public void Update()
	{
		// health hack 
        if (Input.GetKey(KeyCode.H))
        {
            isHpress = true;
        }
        if (isHpress && Input.GetKey(KeyCode.E))
        {
            isEpress = true;
        }
        if (isEpress && Input.GetKey(KeyCode.A))
        {
            isApress = true;
        }
        if (isApress && Input.GetKey(KeyCode.L))
        {
            isLpress = true;
        }
        if (isLpress && Input.GetKey(KeyCode.T))
        {
            isTpress = true;
        }
        if (isTpress && Input.GetKey(KeyCode.H))
        {
            isHpress = false;
            isEpress = false;
            isLpress = false;
            isTpress = false;
			AddBlocksHealth();
        }
		
        // build hack
        if (Input.GetKey(KeyCode.B) && !has_tower)
        {
            isBpress = true;
        }
        if (isBpress && Input.GetKey(KeyCode.U))
        {
            isUpress = true;
        }
        if (isUpress && Input.GetKey(KeyCode.I))
        {
            isIpress = true;
        }
        if (isIpress && Input.GetKey(KeyCode.L))
        {
            isLpress2 = true;
        }
        if (isIpress && Input.GetKey(KeyCode.E))
        {
            isBpress = false;
            isUpress = false;
            isIpress = false;
            isLpress2 = false;
			has_tower = true;
            BuildCrazyTower();
        }
	}
	
	public void AddBlocksHealth()
	{
		GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();
		// go over all the objects
		foreach(GameObject go in allObjects)
		{
			// if object is shows to be a block
			if (go.activeInHierarchy && go.name.Contains("block"))
			{
				go.GetComponent<BlockColaider>().health = 100;
			}				
		}
	}
	
	public void BuildCrazyTower()
	{
        if (!this.has_tower)
        {
            GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();
            // go over all the objects
            foreach (GameObject go in allObjects)
            {
                // if object is shows to be enemy or block
                if (go.name.Contains("block") || go.name.Contains("enemy"))
                {
                    Destroy(go);
                }
            }
        }
		secretTower.SetActive(true);
        this.has_tower = true;
    }
}