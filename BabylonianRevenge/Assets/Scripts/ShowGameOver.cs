﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowGameOver : MonoBehaviour
{
    public void GameOver()
    {
        // TODO: finish here
        GameObject.Find("game_over_menu/canvas/time_txt").GetComponent<TextMesh>().text = 1.ToString();
        GameObject.Find("game_over_menu/canvas/blocks_txt").GetComponent<TextMesh>().text = 1.ToString();
        GameObject.Find("game_over_menu/canvas/enemy_txt").GetComponent<TextMesh>().text = 1.ToString();
        GameObject.Find("game_over_menu/canvas/heigth_txt").GetComponent<TextMesh>().text = 1.ToString();
        Time.timeScale = 0;
    }
}
