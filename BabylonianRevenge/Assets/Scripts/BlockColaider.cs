﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockColaider : MonoBehaviour
{
    // members //
    public int health;
    public bool inPlace;
    public GameManager gm;
    // end - membres //

    void Start()
    {
        this.gm = GameObject.Find("/game_manager").GetComponent<GameManager>();
        this.inPlace = false;

        switch (gm.gameLevel)
        {
            case GameLevel.Easy:
                this.health = 2;
                break;
            case GameLevel.Medium:
                this.health = 1;
                break;
            case GameLevel.Hard:
                this.health = 0;
                break;
        }
    }

    void Update()
    {
    }

    void OnCollisionEnter(Collision collision)
    {
        // check if in place - if so, add to the game manager and don't do it again
        if (!this.inPlace && ((collision.gameObject.name.Contains("block") && collision.gameObject.GetComponent<BlockColaider>().inPlace) || collision.gameObject.name.Contains("floor")))
        {
            this.inPlace = true;
            gm.AddBlock(gameObject);
        }

        // check if dead upon hit 
        if (this.inPlace && collision.gameObject.name.Contains("enemy"))
        {
            if (health == 0)
            {
                ContactPoint contact = collision.contacts[0];
                Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
                Vector3 pos = contact.point;
                GameObject explosionPrefab = Resources.Load("block_explosion", typeof(GameObject)) as GameObject;
                GameObject explosionObj = Instantiate(explosionPrefab, pos, rot);
                explosionObj.AddComponent<BombEvent>();
                gm.removeBlock(gameObject);
                Destroy(gameObject);
            }
            else
            {
                health -= 1;
            }
            // if enemy - kill it //
            // remove from the list of avalible enemies
            gm.removeEnemy(collision.gameObject);
            // make a dieing animetion
            bool needKillAnimetion = true;
            try
            {
                collision.gameObject.GetComponent<Animator>().SetBool("is_die", true);
            }
            catch (Exception error)
            {
                needKillAnimetion = false;
            }
            // stop moving
            collision.gameObject.GetComponent<EnemyLogic>().isActive = false;
            // move backward 
            var heading = (gameObject.transform.position - collision.gameObject.transform.position);
            collision.gameObject.transform.position -= heading / heading.magnitude;
            // remove from the game
            if (needKillAnimetion)
            {
                Destroy(collision.gameObject, 1.2f);
            }
            else
            {
                Destroy(collision.gameObject, 0.05f);
            }

            // vibrate
            SettingsManager sm = GameObject.Find("settings_manager").GetComponent<SettingsManager>();
            if(sm.vibToggle) {
                Handheld.Vibrate();
            }
        }
    }
}
