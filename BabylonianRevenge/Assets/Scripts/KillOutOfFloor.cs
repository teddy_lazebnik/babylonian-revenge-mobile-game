using System;
using System.Collections;
using UnityEngine;

class KillOutOfFloor : MonoBehaviour
{
	public GameManager gameManager;

	private void OnTriggerEnter(Collider other)
	{
		if(other.tag.CompareTo("Enemy") == 0) {
			gameManager.removeEnemy(other.gameObject);
		} else {
			gameManager.removeBlock(other.gameObject);
		}
		Destroy(other.gameObject);
	}
}