﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumToValue
{
    public static string getValue(SceneNames name)
    {
        switch (name)
        {
            case SceneNames.Game:
                return "Game";
            case SceneNames.Menu:
                return "Menu";
            case SceneNames.GameBuild:
                return "GameBuild";
            default:
                return "";
        }
    }
    public static int getValue(GameLevel name)
    {
        switch (name)
        {
            case GameLevel.Easy:
                return 1;
            case GameLevel.Medium:
                return 2;
            case GameLevel.Hard:
                return 3;
            default:
                return 2;
        }
    }
}
