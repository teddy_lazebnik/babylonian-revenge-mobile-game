﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelBtnScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Button btn = gameObject.GetComponent<Button>() as Button;
        btn.onClick.AddListener(SetLevel);
    }


    // Update is called once per frame
    void SetLevel()
    {
        SettingsManager manager = GameObject.Find("SettingsManager").GetComponent<SettingsManager>() as SettingsManager;
        switch(gameObject.name) {
            case "level_btn_easy":
                manager.ChangeDifficultyEasy();
                break;
            case "level_btn_medium":
                manager.ChangeDifficultyMedium();
                break;
            case "level_btn_hard":
                manager.ChangeDifficultyHard();
                break;
        }
    }
}
