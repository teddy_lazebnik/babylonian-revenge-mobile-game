﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean.Touch;

public class PickManager : MonoBehaviour
{
    // consts //
    public const int PICK_BUTTONS_COUNT = 5;
    public const float SIZE_FACTOR = 0.2f;
    public const float delayCheck = 1f;
    // end - consts //

    // members //
    public GameObject pickPanel;
    public GameObject finishBtn;
    public GameObject currentObject;
    public Camera dragCamera;
    public GameObject gameWolrd;

    public List<GameObject> pickerBtns;
    public List<Block> blocksPickers;
    public List<Texture> buttonsImages;
    public GameManager gameManager;
    // end - members //

    // Start is called before the first frame update
    void Start()
    {
        this.pickPanel = GameObject.Find("/game/Main Camera/Canvas/pick_panel");
        this.finishBtn = GameObject.Find("/game/Main Camera/Canvas/put_btn");
        for (int i = 1; i < PICK_BUTTONS_COUNT + 1; ++i)
        {
            GameObject currentPickerBtn = GameObject.Find("/game/Main Camera/Canvas/pick_panel/pick_btn_" + i.ToString());
            if (currentPickerBtn.activeInHierarchy)
            {
                this.pickerBtns.Add(currentPickerBtn);
            }
        }
    }

    public void PickEvent(int index)
    {
        string prefabName = GameObject.Find("/game/Main Camera/Canvas/pick_panel/pick_btn_" + index.ToString()).GetComponent<Block>().blockName;
        GameObject instance = Resources.Load("Blocks/" + prefabName, typeof(GameObject)) as GameObject;
        this.currentObject = Instantiate(instance,
            this.gameManager.centerFloor + Vector3.up * (this.gameManager.towerSize + 15),
            Quaternion.identity);
        this.currentObject.AddComponent<BlockColaider>();
        this.currentObject.GetComponent<Rigidbody>().useGravity = false;
        this.currentObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
        currentObject.transform.parent = gameWolrd.transform;
        this.pickPanel.SetActive(false);
        this.finishBtn.SetActive(true);
    }


    public void PutEvent()
    {
        this.currentObject.GetComponent<Rigidbody>().useGravity = true;
        this.currentObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationZ;
        Destroy(this.currentObject.GetComponent<LeanDragTranslate>());
        Destroy(this.currentObject.GetComponent<LeanTwistRotate>());
        Destroy(this.currentObject.GetComponent<LeanTwistRotateAxis>());
        this.finishBtn.SetActive(false);
        Invoke("PutEventReadyNext", delayCheck);
    }

    public void PutEventReadyNext()
    {
        this.pickPanel.SetActive(true);
        GenerateNewBlocks();
        this.currentObject = null;
    }

    /*
	*	Pick different set of blocks each time for the tower
	*/
    public void GenerateNewBlocks()
    {
        try
        {
            changeAllButtons(randomPickBlocks(pickerBtns.Count));
        }
        catch (Exception exception)
        {
            print(exception.Message);
        }
    }

    private List<string> randomPickBlocks(int amount)
    {
        List<string> answer = new List<string>();
        List<int> indexes = new List<int>();
        System.Random random = new System.Random();
        while (answer.Count < amount)
        {
            // get new index
            int pickedIndex = random.Next(blocksPickers.Count);
            // check if we don't have this index
            bool isAlreadyPicked = false;
            for (int pastIndex = 0; pastIndex < indexes.Count; ++pastIndex)
            {
                if (pickedIndex == indexes[pastIndex])
                {
                    isAlreadyPicked = true;
                }
            }
            // if new index 
            if (!isAlreadyPicked)
            {
                // add index
                indexes.Add(pickedIndex);
                answer.Add(blocksPickers[pickedIndex].blockName);
            }
        }
        return answer;
    }

    private void changeAllButtons(List<string> blockNames)
    {
        // if sizes don't agree we cannot do this
        if (pickerBtns.Count != blockNames.Count)
        {
            throw new Exception("Block must agree with the possible buttons");
        }

        // change each button in the array
        for (int index = 0; index < pickerBtns.Count; ++index)
        {
            try
            {
                changeButton(pickerBtns[index], blockNames[index]);
            }
            catch (Exception changeButtonError)
            {
                print(changeButtonError.Message);
            }
        }
    }

    private void changeButton(GameObject btn, string blockName)
    {
        Texture image = this.buttonsImages[0];
        for (int index = 0; index < blocksPickers.Count; ++index)
        {
            if (blocksPickers[index].blockName == blockName)
            {
                image = this.buttonsImages[index];
            }
        }
        Button classBtn = btn.GetComponent<Button>();
        classBtn.GetComponent<RawImage>().texture = image;
        classBtn.GetComponent<Block>().blockName = blockName;
    }

}
