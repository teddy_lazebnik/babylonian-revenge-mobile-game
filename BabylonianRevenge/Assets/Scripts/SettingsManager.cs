﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    private static SettingsManager _instance;
    public AudioMixer audioMixer;
    public Toggle vibToggle;

    [SerializeField]
    public bool vibration;
    public GameLevel difficulty;
    public float volume;
    [SerializeField]
    private bool isMute;

    public static SettingsManager Instance { get { return _instance; } }

    private void Awake()
    {
        if(_instance != null && _instance != this) {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }

    private void Start() {
        isMute = false;
        DontDestroyOnLoad(gameObject);
    }

    public void SetToggle() {
        vibToggle = GameObject.Find("Toggle").GetComponent<Toggle>();
        vibToggle.isOn = vibration;
        DontDestroyOnLoad(vibToggle);
    }
    
    public void SetVolume(float vol) {
        volume = vol;
        audioMixer.SetFloat("Volume", vol);
    }

    public bool isSoundMute() {
        return isMute;
    }

    public void Mute() {
        if(!isMute) {
            audioMixer.SetFloat("Volume", -80f);
            // volume = -80f;
            isMute = true;
        } else {
            audioMixer.SetFloat("Volume", volume);
            isMute = false;
        }
    }

    public void SetVibration(bool vib) {
        vibration = vib;
    }

    public void ChangeDifficultyEasy() {
        difficulty = GameLevel.Easy;
        ChangeLevelButtonVisibility(difficulty);
    }

    public void ChangeDifficultyMedium() {
        difficulty = GameLevel.Medium;
        ChangeLevelButtonVisibility(difficulty);
    }

    public void ChangeDifficultyHard() {
        difficulty = GameLevel.Hard;
        ChangeLevelButtonVisibility(difficulty);
    }

    private void ChangeLevelButtonVisibility(GameLevel level) {
        var btnEasy = GameObject.Find("level_btn_easy").GetComponent<Image>();
        var btnMedium = GameObject.Find("level_btn_medium").GetComponent<Image>();
        var btnHard = GameObject.Find("level_btn_hard").GetComponent<Image>();
        Color tempColor;
        switch(level) {
            case GameLevel.Easy:
                tempColor = btnEasy.color;
                tempColor.a = 0.5f;
                btnEasy.color = tempColor;

                tempColor = btnMedium.color;
                tempColor.a = 1f;
                btnMedium.color = tempColor;

                tempColor = btnHard.color;
                tempColor.a = 1f;
                btnHard.color = tempColor;
                break;
            case GameLevel.Medium:
                tempColor = btnEasy.color;
                tempColor.a = 1f;
                btnEasy.color = tempColor;

                tempColor = btnMedium.color;
                tempColor.a = 0.5f;
                btnMedium.color = tempColor;

                tempColor = btnHard.color;
                tempColor.a = 1f;
                btnHard.color = tempColor;
                break;
            case GameLevel.Hard:
                tempColor = btnEasy.color;
                tempColor.a = 1f;
                btnEasy.color = tempColor;

                tempColor = btnMedium.color;
                tempColor.a = 1f;
                btnMedium.color = tempColor;

                tempColor = btnHard.color;
                tempColor.a = 0.5f;
                btnHard.color = tempColor;
                break;
        }
    }
}
