﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombEvent : MonoBehaviour
{
    public static float secondsEventDelay = 0.5f;

    // Start is called before the first frame update
    void Start() => Invoke("killObject", secondsEventDelay);

    public void killObject() => Destroy(gameObject);
}
