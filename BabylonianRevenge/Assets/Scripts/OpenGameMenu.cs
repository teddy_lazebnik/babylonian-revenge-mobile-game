﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OpenGameMenu : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData pointerEventData)
    {
        ClickEvent();
    }

    void OnMouseDown()
    {
        ClickEvent();
    }

    public void ClickEvent()
    {
        // show pause menu + stop the clock
        GameObject.Find("pause_menu").SetActive(true);
        Time.timeScale = 0;
    }
}
