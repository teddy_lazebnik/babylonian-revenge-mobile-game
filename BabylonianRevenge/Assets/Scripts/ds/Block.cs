﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    // members //
    public string blockName;
    public string blockImagePath;
    public GameObject blockObj;
    public Vector3 size;
    // end - members //
    
    public Sprite getImageToBtn()
    {
        // if we don't have the path then we lost
        if (blockImagePath == null || blockImagePath == "")
        {
            throw new Exception("blockImagePath is not set");
        }

        // load the resource from the path 
        return Resources.Load<Sprite>("BlocksImages/" + this.blockImagePath + ".png");
    }
}