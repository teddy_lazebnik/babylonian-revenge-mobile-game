﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLogic : MonoBehaviour
{
    public static float colddownTimeSeconds = 0.4f;
    public static float enemySpeed = 7.5f;
    public static float maxTouchErrorDistance = 650f;

    // members //
    public GameObject target;
    public GameManager gm;
    public bool isActive;

    public Animator animator;
    // end - members //

    // Start is called before the first frame update
    void Start()
    {
        this.gm = GameObject.Find("/game_manager").GetComponent<GameManager>();
        this.isActive = false;
        this.target = null;
        try
        {
            this.animator = gameObject.GetComponent<Animator>();
        }
        catch (Exception error)
        {
            this.animator = null;
        }
        Invoke("makeReady", colddownTimeSeconds);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.isActive && this.gm.blocks.Count > 0)
        {
            try
            {
                moveToTarget();
            }
            catch (Exception exception)
            {
                this.target = findTarget();
            }
        }
    }

    /// <summary>
    /// Find a block to kill 
    /// </summary>
    /// <returns></returns>
    public GameObject findTarget()
    {
        try
        {
            this.animator.SetBool("is_moving", false);
        }
        catch (Exception error)
        {

        }

        float distance = 100f;
        List<GameObject> visibaleBlocks = new List<GameObject>();

        // go over each block and remember the once the player can see and hit
        for (int i = 0; i < this.gm.blocks.Count; i++)
        {
            // check if right height - not, just jump over
            /*
            if (Math.Abs(gameObject.transform.position.y - this.gm.blocks[i].transform.position.y) > 1)
            {
                continue;
            }
            */

            // check if can see
            RaycastHit hit;
            if (Physics.Linecast(gameObject.transform.position, this.gm.blocks[i].transform.position, out hit))
            {
                if (hit.collider.attachedRigidbody.name.Contains("block"))
                {
                    visibaleBlocks.Add(this.gm.blocks[i]);
                }
            }
        }

        if (visibaleBlocks.Count > 0)
        {
            // order by distance
            visibaleBlocks = visibaleBlocks.OrderBy( x => Vector3.Distance(this.transform.position, x.transform.position)).ToList();
            return visibaleBlocks[0];
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Move to a target to kill
    /// </summary>
    /// <returns></returns>
    public void moveToTarget()
    {
        gameObject.transform.LookAt(this.target.transform);
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, this.target.transform.position, enemySpeed * Time.deltaTime);

        try
        {
            this.animator.SetBool("is_moving", true);
        }
        catch (Exception error)
        {
            
        }
    }

    /// <summary>
    /// make the enemy ready
    /// </summary>
    public void makeReady()
    {
        this.isActive = true;
    }
}
