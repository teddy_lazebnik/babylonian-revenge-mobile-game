﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyKillManager : MonoBehaviour
{
    // consts //
    public static float maxTouchErrorDistance = 40f;
    // end - consts //

    // members //
    public GameManager gm;
    public Camera mainCamera;
    // end - members //

    public void OnTapDestroy(Lean.Touch.LeanFinger pos)
    {
        // init stored data
        float bestDistance = 1 + EnemyKillManager.maxTouchErrorDistance;
        GameObject bestEnemy = null;
        
        // calculate the distance between every enemy and the touch position
        Vector2 touchPos = new Vector2(pos.ScreenPosition.x, pos.ScreenPosition.y);

        // go over all enemy objects
        float newDistance = 0;
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            newDistance = Vector2.Distance(touchPos, mainCamera.WorldToScreenPoint(obj.transform.position));
            // if the touch is of other enemy - isTappedObject is false for this enemy
            if (bestDistance > newDistance)
            {
                bestDistance = newDistance;
                bestEnemy = obj;
            }
        }

        // execute destroy only if this object is the tapped enemy is found and close enoght
        if (bestDistance < EnemyKillManager.maxTouchErrorDistance )
        {
            var sound = GameObject.Find("sound_manager");
            sound.GetComponent<SoundFXs>().ExlposionSound();
            // explode
            GameObject explosionPrefab = Resources.Load("BigExplosionEffect", typeof(GameObject)) as GameObject;
            GameObject explosionObj = Instantiate(explosionPrefab, bestEnemy.transform.position, bestEnemy.transform.rotation);
            Destroy(explosionObj, 3f);
            // remove enemy
            try
            {
                gm.removeEnemy(bestEnemy);
            }
            catch (Exception error)
            {
                print(error.Message);
            }
            Destroy(bestEnemy);
        }
    }
}
