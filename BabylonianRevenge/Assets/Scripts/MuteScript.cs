﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteScript : MonoBehaviour
{
    private void Start() {
        Button btn = gameObject.GetComponent<Button>() as Button;
        btn.onClick.AddListener(Mute);
    }
    
    void Mute() {
        var SettingsManager = GameObject.Find("SettingsManager").GetComponent<SettingsManager>();
        SettingsManager.Mute();
    }
}
