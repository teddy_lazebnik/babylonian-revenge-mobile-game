﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsManager : MonoBehaviour
{
    public GameManager gameManager;
    public Text size_txt_obj;

    // Start is called before the first frame update
    void Start()
    {
        // size_txt_obj = GameObject.Find("/game/Main Camera/size_txt");
        // gameManager = GameObject.Find("game_manager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        float value = gameManager.towerSize;
        value = (float)Math.Round(value * 100) / 100;
        size_txt_obj.text = value.ToString() + "M";
    }
}
